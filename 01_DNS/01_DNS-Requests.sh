
#Simple DNS Request
nslookup moodle.fh-burgenland.at


#MX Request incl. further requests
nslookup -type=MX bmi.gv.at
nslookup mx.bmi.gv.at

#All entry types
nslookup -type=ANY youtube.com