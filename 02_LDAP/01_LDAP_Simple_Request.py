from ldap3 import Server, Connection, ALL, NTLM

# Define the server and connection details
server = Server('ldap://tevs-main-01.untrace.it', get_info=ALL)
conn = Connection(server, user='cn=admin,dc=example,dc=org', password='admin')

# Bind to the server
if not conn.bind():
    print('Error in binding to server:', conn.result)
else:
    print('Bind successful')

    # Define the search parameters (search for users in group 501 (users) - 500(admins)
    search_base = 'dc=example,dc=org'
    search_filter = '(&(objectClass=inetOrgPerson)(gidNumber=501))'
    search_attributes = ['cn', 'sn', 'mail']

    # Perform the search
    conn.search(search_base, search_filter, attributes=search_attributes)
    # Print the results
    print("All Non-Admin Users:")
    print('-'*20)
    for entry in conn.entries:
        print(entry)

    search_filter = '(&(objectClass=inetOrgPerson)(gidNumber=500))'
    # Perform the search
    conn.search(search_base, search_filter, attributes=search_attributes)
    # Print the results
    print("All Admin Users:")
    print('-'*20)
    for entry in conn.entries:
        print(entry)

    # Unbind the connection
    conn.unbind()
