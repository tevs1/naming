from ldap3 import Server, Connection, ALL, NTLM

# Define the server and connection details
server = Server('ldap://tevs-main-01.untrace.it', get_info=ALL)
conn = Connection(server, user='cn=admin,dc=example,dc=org', password='admin')

# Bind to the server
if not conn.bind():
    print('Error in binding to server:', conn.result)
else:
    print('Bind successful')

    # Define the search parameters (search for users in group 501 (users) - 500(admins)
    search_base = 'ou=statusservers,ou=Servers,dc=example,dc=org'
    search_filter = '(objectClass=device)'
    search_attributes = ['cn']

    # Perform the search
    conn.search(search_base, search_filter, attributes=search_attributes)
    # Print the results
    print("All Statusservers:")
    print('-'*20)
    for entry in conn.entries:
        print(entry)
    # Unbind the connection
    conn.unbind()
